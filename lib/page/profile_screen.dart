import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  String _imagePath = "";
  File? _imageFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Thông tin cá nhân',style: TextStyle(fontWeight: FontWeight.w500),),
        actions: [
          IconButton(onPressed: (){

          }, icon: const Icon(Icons.more_vert,))
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Center(
                child: Stack(
                  fit: StackFit.loose,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: (_imagePath.isEmpty) ? Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ) : null,
                      ),
                      child:   CircleAvatar(
                        backgroundImage: (_imagePath.isEmpty) ?
                        const AssetImage('assets/images/image_avatar.jpg') : FileImage(_imageFile!) as ImageProvider,
                        radius: 50,
                      )),
                    Positioned(
                        right: 0,
                        bottom: 0,
                        child: InkWell(
                          onTap: (){_pickImage();},
                          child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white, width: 2),
                                  shape: BoxShape.circle,
                                  color: Colors.grey.shade400),
                              child: const Padding(
                                padding: EdgeInsets.all(6.0),
                                child: Icon(Icons.camera_alt,size: 20,),
                              )),
                        ))
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Future<void> _pickImage() async {
    var pickedFile = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imagePath = pickedFile.path;
        _imageFile = File(_imagePath);
      });
    } else {
      print('No image selected.');
    }
  }
}
